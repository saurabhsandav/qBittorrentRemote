// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {
    repositories {
        google()
        jcenter()
    }
    dependencies {
        classpath(Deps.Gradle.PLUGIN)
        classpath(kotlin(Deps.Kotlin.GRADLE_PLUGIN, Deps.Kotlin.VERSION))

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter()

        // TODO Temporary workaround (Unable to resolve dependencies)
        // https://github.com/afollestad/material-dialogs/issues/1575
        maven { url = uri("https://dl.bintray.com/drummer-aidan/maven/") }
    }
}

task<Delete>("clean") {
    delete(rootProject.buildDir)
}
