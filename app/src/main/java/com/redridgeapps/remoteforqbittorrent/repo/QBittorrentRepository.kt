package com.redridgeapps.remoteforqbittorrent.repo

import com.github.kittinunf.result.Result
import com.redridgeapps.remoteforqbittorrent.api.QBittorrentService
import com.redridgeapps.remoteforqbittorrent.model.QBittorrentLog
import com.redridgeapps.remoteforqbittorrent.model.Torrent
import com.redridgeapps.remoteforqbittorrent.util.MIME_TYPE_TORRENT_FILE
import kotlinx.coroutines.experimental.Deferred
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import java.io.File
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QBittorrentRepository @Inject constructor(
        private val prefRepo: PreferenceRepository,
        private val qBitService: QBittorrentService
) {

    fun saveConfig(
            host: String,
            port: Int,
            useHttps: Boolean,
            usernameStr: String,
            passwordStr: String
    ) = prefRepo.apply {
        baseUrl = QBittorrentService.buildBaseURL(host, port, useHttps)
        username = usernameStr
        password = passwordStr
    }

    suspend fun login(): Result<Unit, Exception> {

        val request = qBitService.login(
                baseUrl = prefRepo.baseUrl,
                username = prefRepo.username,
                password = prefRepo.password
        )

        return request.processResponse(this::extractSIDAndSave)
    }

    suspend fun getTorrentList(
            filter: String? = null
    ): Result<List<Torrent>, Exception> {

        val request = qBitService.getTorrentList(
                baseUrl = prefRepo.baseUrl,
                filter = filter,
                sort = prefRepo.torrentListSort,
                reverse = prefRepo.torrentListSortReverse
        )

        return request.processResult { it ?: ArrayList() }
    }

    suspend fun pauseAll(): Result<Unit, Exception> {
        val request = qBitService.pauseAll(prefRepo.baseUrl)

        return request.processResult { Unit }
    }

    suspend fun resumeAll(): Result<Unit, Exception> {
        val request = qBitService.resumeAll(prefRepo.baseUrl)

        return request.processResult { Unit }
    }

    suspend fun addTorrentLinks(links: List<String>): Result<Unit, Exception> {
        val request = qBitService.downloadFromURL(
                baseUrl = prefRepo.baseUrl,
                urls = links.joinToString("\n")
        )

        return request.processResult { Unit }
    }

    suspend fun addTorrentFiles(files: List<File>): Result<Unit, Exception> {
        val torrents = files.map {
            val requestFile = RequestBody.create(MediaType.parse(MIME_TYPE_TORRENT_FILE), it)
            MultipartBody.Part.createFormData(LABEL_PARAMETER_NAME_TORRENTS, it.name, requestFile)
        }

        val request = qBitService.uploadFromDisk(
                baseUrl = prefRepo.baseUrl,
                torrents = torrents
        )

        return request.processResult { Unit }
    }

    suspend fun getLog(
            normal: Boolean = true,
            info: Boolean = true,
            warning: Boolean = true,
            critical: Boolean = true,
            lastId: Int = -1
    ): Result<List<QBittorrentLog>, Exception> {
        val request = qBitService.getLog(
                baseUrl = prefRepo.baseUrl,
                normal = normal,
                info = info,
                warning = warning,
                critical = critical,
                lastKnownId = lastId
        )

        return request.processResult { it ?: ArrayList() }
    }

    private fun extractSIDAndSave(resp: Response<String>) {

        val setCookieStr = resp.headers().get(LABEL_HEADER_SET_COOKIE).toString()
        val sid = Regex(SID_REGEX_PATTERN)
                .find(setCookieStr)
                ?.groupValues
                ?.get(1)

        prefRepo.initialConfigFinished = true
        prefRepo.sid = "SID=$sid"
    }

    private suspend fun <T, R : Any> Deferred<Response<T>>.processResponse(
            onSuccess: ((Response<T>) -> R)
    ): Result<R, Exception> {
        return try {
            Result.of(onSuccess(await()))
        } catch (e: Exception) {
            Result.error(e)
        }
    }

    private suspend fun <T, R : Any> Deferred<T>.processResult(
            onSuccess: ((T?) -> R)
    ): Result<R, Exception> {
        return try {
            Result.of(onSuccess(await()))
        } catch (e: Exception) {
            Result.error(e)
        }
    }
}

private const val LABEL_HEADER_SET_COOKIE = "Set-Cookie"
private const val LABEL_PARAMETER_NAME_TORRENTS = "torrents"
private const val SID_REGEX_PATTERN = """SID=(.+?);"""
