package com.redridgeapps.remoteforqbittorrent.ui.config

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.redridgeapps.remoteforqbittorrent.repo.QBittorrentRepository
import com.redridgeapps.remoteforqbittorrent.ui.base.BaseViewModel
import com.redridgeapps.remoteforqbittorrent.util.asMutable
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import javax.inject.Inject

class ConfigViewModel @Inject constructor(
        private val qBitRepo: QBittorrentRepository
) : BaseViewModel() {

    val loginResultLiveData: LiveData<Result<Unit, Exception>> = MutableLiveData()

    fun login(
            host: String,
            port: Int,
            useHttps: Boolean,
            username: String,
            password: String
    ) = launch(UI, parent = job) {
        qBitRepo.saveConfig(host, port, useHttps, username, password)

        val result = qBitRepo.login()

        loginResultLiveData.asMutable().postValue(result)
    }
}
