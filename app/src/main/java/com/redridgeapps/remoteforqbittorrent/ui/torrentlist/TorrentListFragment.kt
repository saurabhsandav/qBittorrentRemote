package com.redridgeapps.remoteforqbittorrent.ui.torrentlist

import android.Manifest
import android.content.ClipDescription
import android.content.ClipboardManager
import android.os.Bundle
import android.text.InputType
import android.view.*
import android.webkit.MimeTypeMap
import androidx.core.content.getSystemService
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.files.FileFilter
import com.afollestad.materialdialogs.files.fileChooser
import com.afollestad.materialdialogs.files.folderChooser
import com.afollestad.materialdialogs.input.input
import com.github.kittinunf.result.failure
import com.github.kittinunf.result.success
import com.redridgeapps.remoteforqbittorrent.R
import com.redridgeapps.remoteforqbittorrent.api.QBittorrentService.Sort
import com.redridgeapps.remoteforqbittorrent.databinding.FragmentTorrentListBinding
import com.redridgeapps.remoteforqbittorrent.ui.base.BaseFragment
import com.redridgeapps.remoteforqbittorrent.ui.base.DrawerActivityContract
import com.redridgeapps.remoteforqbittorrent.util.MIME_TYPE_TORRENT_FILE
import com.redridgeapps.remoteforqbittorrent.util.getViewModel
import java.io.File
import javax.inject.Inject

class TorrentListFragment : BaseFragment() {

    @Inject
    lateinit var torrentListAdapter: TorrentListAdapter

    private lateinit var binding: FragmentTorrentListBinding
    private val viewModel by lazy { getViewModel(TorrentListViewModel::class.java) }

    private val fileFilter: (File) -> Boolean = {
        MimeTypeMap.getSingleton().getMimeTypeFromExtension(it.extension) == MIME_TYPE_TORRENT_FILE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_torrent_list, container, false)

        binding.srl.setOnRefreshListener { viewModel.refreshTorrentList() }
        binding.srl.isRefreshing = true

        setupRecyclerView()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeActivity()
        observeViewModel()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.torrentlist_options_menu, menu)

        val id = when (viewModel.listSort) {
            Sort.NAME -> R.id.action_sort_name
            Sort.SIZE -> R.id.action_sort_size
            Sort.ETA -> R.id.action_sort_eta
            Sort.PRIORITY -> R.id.action_sort_priority
            Sort.PROGRESS -> R.id.action_sort_progress
            Sort.RATIO -> R.id.action_sort_ratio
            Sort.STATE -> R.id.action_sort_state
            Sort.DLSPEED -> R.id.action_sort_download_speed
            Sort.UPSPEED -> R.id.action_sort_upload_speed
            else -> throw IllegalStateException("Unknown argument: ${viewModel.listSort}")
        }

        menu?.findItem(id)?.isChecked = true
        menu?.findItem(R.id.action_sort_reverse)?.isChecked = viewModel.listSortReverse
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        var result = true

        when (item.itemId) {
            R.id.action_add_link -> addTorrentLink()
            R.id.action_add_file -> addTorrentFile()
            R.id.action_add_folder -> addTorrentFilesFromFolder()
            R.id.action_sort_name -> setSort(item, Sort.NAME)
            R.id.action_sort_size -> setSort(item, Sort.SIZE)
            R.id.action_sort_eta -> setSort(item, Sort.ETA)
            R.id.action_sort_priority -> setSort(item, Sort.PRIORITY)
            R.id.action_sort_progress -> setSort(item, Sort.PROGRESS)
            R.id.action_sort_ratio -> setSort(item, Sort.RATIO)
            R.id.action_sort_state -> setSort(item, Sort.STATE)
            R.id.action_sort_download_speed -> setSort(item, Sort.DLSPEED)
            R.id.action_sort_upload_speed -> setSort(item, Sort.UPSPEED)
            R.id.action_sort_reverse -> setSortReverse(item)
            R.id.action_pause_all -> viewModel.pauseAll()
            R.id.action_resume_all -> viewModel.resumeAll()
            else -> result = super.onOptionsItemSelected(item)
        }

        return result
    }

    private fun observeActivity() {
        val contractActivity = activity as? DrawerActivityContract ?: return

        contractActivity.navigationItemSelectionsLiveData.observe(this, Observer {
            viewModel.filter = it
        })
    }

    private fun observeViewModel() {
        observeGenericOperations()
        observeTorrentList()
    }

    private fun observeGenericOperations() {
        viewModel.genericOpResultLiveData.observe(this, Observer { result ->
            result.failure { showError(R.string.error_generic) }
        })
    }

    private fun observeTorrentList() {
        viewModel.torrentListLiveData.observe(this, Observer { result ->
            result.success(torrentListAdapter::submitList)
            result.failure { showError(R.string.error_generic) }

            binding.srl.isRefreshing = false
        })
    }

    private fun setupRecyclerView() = binding.recyclerView.apply {
        val linearLayoutManager = LinearLayoutManager(requireContext())

        setHasFixedSize(true)
        adapter = torrentListAdapter
        layoutManager = linearLayoutManager
        addItemDecoration(DividerItemDecoration(requireContext(), linearLayoutManager.orientation))
    }

    private fun setSort(item: MenuItem, sort: String): Boolean {
        item.isChecked = true
        viewModel.listSort = sort
        return true
    }

    private fun setSortReverse(item: MenuItem): Boolean {
        item.isChecked = !item.isChecked
        viewModel.listSortReverse = item.isChecked
        return true
    }

    private fun addTorrentLink() {
        val prefill = getLinkFromClipboard()
        MaterialDialog(requireContext())
                .title(R.string.torrentlist_dialog_label_add_link)
                .input(
                        hint = "http://, https://, magnet: or bc://bt/",
                        prefill = prefill,
                        inputType = InputType.TYPE_CLASS_TEXT
                ) { _, text -> viewModel.addTorrentLinks(listOf(text.toString())) }
                .positiveButton(android.R.string.ok)
                .negativeButton(android.R.string.cancel)
                .show()
    }

    private fun getLinkFromClipboard(): String? {

        val clipboard = requireActivity().getSystemService<ClipboardManager>() ?: return null
        if (!clipboard.hasPrimaryClip()) return null
        val primaryClipDescription = clipboard.primaryClipDescription ?: return null
        val primaryClip = clipboard.primaryClip ?: return null

        var link: String? = null

        if (primaryClipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN) ||
                primaryClipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_HTML)
        ) {
            link = primaryClip.getItemAt(0)?.text?.toString()?.trim()
        } else if (primaryClipDescription.hasMimeType(ClipDescription.MIMETYPE_TEXT_URILIST)) {
            link = primaryClip.getItemAt(0)?.uri?.toString()
        }

        return link?.takeIf {
            LIST_SUPPORTED_LINKS
                    .any { protocol -> it.startsWith(protocol) } || it.matches(Regex(INFO_HASH_PATTERN))
        }
    }

    private fun addTorrentFile() = withPermission(
            permission = Manifest.permission.READ_EXTERNAL_STORAGE,
            errorResId = R.string.error_need_read_storage_permission
    ) {
        val filter: FileFilter = { it.isDirectory || fileFilter(it) }

        MaterialDialog(requireContext())
                .fileChooser(filter = filter) { _, file -> viewModel.addTorrentFiles(listOf(file)) }
                .negativeButton(android.R.string.cancel)
                .show()
    }

    private fun addTorrentFilesFromFolder() = withPermission(
            permission = Manifest.permission.READ_EXTERNAL_STORAGE,
            errorResId = R.string.error_need_read_storage_permission
    ) {
        MaterialDialog(requireContext())
                .folderChooser { _, file ->
                    val fileList = file.listFiles().toList().filter(fileFilter)
                    viewModel.addTorrentFiles(fileList)
                }
                .negativeButton(android.R.string.cancel)
                .show()
    }
}

private const val INFO_HASH_PATTERN = """\b[0-9a-fA-F]{40}\b"""
private val LIST_SUPPORTED_LINKS = listOf("http://", "https://", "magnet:", "bc://bt/")
