package com.redridgeapps.remoteforqbittorrent.api

import com.redridgeapps.remoteforqbittorrent.repo.PreferenceRepository
import com.redridgeapps.remoteforqbittorrent.repo.QBittorrentRepository
import dagger.Lazy
import kotlinx.coroutines.experimental.runBlocking
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QBittorrentInterceptor @Inject constructor(
        private val qBitRepo: Lazy<QBittorrentRepository>,
        private val prefRepo: PreferenceRepository
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val origRequest = chain.request()

        val requestWithAuth = origRequest.newBuilder()
                .addHeader(HEADER_LABEL_COOKIE, prefRepo.sid)
                .build()

        val origResponse = chain.proceed(requestWithAuth)

        if (origResponse.code() == 403 &&
                !origRequest.url().pathSegments().contains(QBittorrentService.POST_LOGIN)) {

            val authResult = runBlocking { qBitRepo.get().login() }

            authResult.component2()?.let { return origResponse }

            val newRequest = origRequest.newBuilder()
                    .addHeader(HEADER_LABEL_COOKIE, prefRepo.sid)
                    .build()

            return chain.proceed(newRequest)
        }

        return origResponse
    }
}

private const val HEADER_LABEL_COOKIE = "Cookie"