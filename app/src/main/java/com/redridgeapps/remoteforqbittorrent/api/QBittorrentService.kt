package com.redridgeapps.remoteforqbittorrent.api

import com.redridgeapps.remoteforqbittorrent.model.QBittorrentLog
import com.redridgeapps.remoteforqbittorrent.model.Torrent
import kotlinx.coroutines.experimental.Deferred
import okhttp3.HttpUrl
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*

interface QBittorrentService {

    companion object {

        const val DEFAULT_BASE_URL = "http://localhost:8080/"
        const val DEFAULT_USERNAME = "admin"
        const val DEFAULT_PASSWORD = "adminadmin"

        const val POST_LOGIN = "login"
        private const val GET_QUERY_TORRENTS = "query/torrents"
        private const val GET_QUERY_GET_LOG = "query/getLog"
        private const val POST_COMMAND_DOWNLOAD = "command/download"
        private const val POST_COMMAND_UPLOAD = "command/upload"
        private const val POST_COMMAND_PAUSE_ALL = "command/pauseAll"
        private const val POST_COMMAND_RESUME_ALL = "command/resumeAll"

        private const val HEADER_LABEL_REFERER = "Referer"

        private const val SCHEME_HTTP = "http"
        private const val SCHEME_HTTPS = "https"

        fun buildBaseURL(host: String, port: Int, useHttps: Boolean): String {
            return HttpUrl.Builder()
                    .scheme(getScheme(useHttps))
                    .host(host)
                    .port(port)
                    .build()
                    .url()
                    .toString()
        }

        private fun getScheme(useHttps: Boolean) = if (useHttps) SCHEME_HTTPS else SCHEME_HTTP

        private fun buildURL(baseUrl: String, path: String): String = baseUrl + path
    }

    object Filter {
        const val ALL = "all"
        const val DOWNLOADING = "downloading"
        const val COMPLETED = "completed"
        const val PAUSED = "paused"
        const val ACTIVE = "active"
        const val INACTIVE = "inactive"
    }

    object Sort {
        const val HASH = "hash"
        const val NAME = "name"
        const val SIZE = "size"
        const val PROGRESS = "progress"
        const val DLSPEED = "dlspeed"
        const val UPSPEED = "upspeed"
        const val PRIORITY = "priority"
        const val NUM_SEEDS = "num_seeds"
        const val NUM_COMPLETE = "num_complete"
        const val NUM_LEECHS = "num_leechs"
        const val NUM_INCOMPLETE = "num_incomplete"
        const val RATIO = "ratio"
        const val ETA = "eta"
        const val STATE = "state"
        const val SEQ_DL = "seq_dl"
        const val F_L_PIECE_PRIO = "f_l_piece_prio"
        const val CATEGORY = "category"
        const val SUPER_SEEDING = "super_seeding"
        const val FORCE_START = "force_start"
    }

    @FormUrlEncoded
    @POST
    fun login(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, POST_LOGIN),
            @Field("username") username: String,
            @Field("password") password: String
    ): Deferred<Response<String>>

    @GET
    fun getTorrentList(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, GET_QUERY_TORRENTS),
            @Query("filter") filter: String? = null,
            @Query("category") category: String? = null,
            @Query("sort") sort: String? = null,
            @Query("reverse") reverse: Boolean? = null,
            @Query("limit") limit: Int? = null,
            @Query("offset") offset: Int? = null
    ): Deferred<List<Torrent>>

    @GET
    fun getLog(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, GET_QUERY_GET_LOG),
            @Query("normal") normal: Boolean? = null,
            @Query("info") info: Boolean? = null,
            @Query("warning") warning: Boolean? = null,
            @Query("critical") critical: Boolean? = null,
            @Query("last_known_id") lastKnownId: Int? = null
    ): Deferred<List<QBittorrentLog>>

    @Multipart
    @POST
    fun downloadFromURL(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, POST_COMMAND_DOWNLOAD),
            @Part("urls") urls: String,
            @Part("savepath") savepath: String? = null,
            @Part("cookie") cookie: String? = null,
            @Part("category") category: String? = null,
            @Part("skip_checking") skip_checking: Boolean? = null,
            @Part("paused") paused: Boolean? = null,
            @Part("root_folder") rootFolder: Boolean? = null,
            @Part("rename") rename: String? = null,
            @Part("upLimit") upLimit: Int? = null,
            @Part("dlLimit") dlLimit: Int? = null,
            @Part("sequentialDownload") sequentialDownload: Boolean? = null,
            @Part("firstLastPiecePrio") firstLastPiecePriority: Boolean? = null
    ): Deferred<Void>

    @Multipart
    @POST
    fun uploadFromDisk(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, POST_COMMAND_UPLOAD),
            @Part torrents: List<MultipartBody.Part>,
            @Part("savepath") savepath: String? = null,
            @Part("cookie") cookie: String? = null,
            @Part("category") category: String? = null,
            @Part("skip_checking") skip_checking: Boolean? = null,
            @Part("paused") paused: Boolean? = null,
            @Part("root_folder") rootFolder: Boolean? = null,
            @Part("rename") rename: String? = null,
            @Part("upLimit") upLimit: Int? = null,
            @Part("dlLimit") dlLimit: Int? = null,
            @Part("sequentialDownload") sequentialDownload: Boolean? = null,
            @Part("firstLastPiecePrio") firstLastPiecePriority: Boolean? = null
    ): Deferred<Void>

    @POST
    fun pauseAll(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, POST_COMMAND_PAUSE_ALL)
    ): Deferred<Void>

    @POST
    fun resumeAll(
            @Header(HEADER_LABEL_REFERER) baseUrl: String,
            @Url url: String = buildURL(baseUrl, POST_COMMAND_RESUME_ALL)
    ): Deferred<Void>
}
