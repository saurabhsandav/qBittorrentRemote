import org.jetbrains.kotlin.gradle.dsl.Coroutines

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-android-extensions")
    id("kotlin-kapt")
}

android {
    compileSdkVersion(ProjectConfig.COMPILE_SDK_VERSION)

    buildToolsVersion = ProjectConfig.BUILD_TOOLS_VERSION

    defaultConfig {
        applicationId = "com.redridgeapps.remoteforqbittorrent"

        minSdkVersion(ProjectConfig.MIN_SDK_VERSION)
        targetSdkVersion(ProjectConfig.TARGET_SDK_VERSION)

        versionCode = 1
        versionName = "0.1"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        vectorDrawables.useSupportLibrary = true
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
        }
    }

    dataBinding {
        isEnabled = true
    }
}

androidExtensions {
    isExperimental = true
}

kotlin {
    experimental.coroutines = Coroutines.ENABLE
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    // Testing
    testImplementation(Deps.Testing.JUNIT)
    androidTestImplementation(Deps.Testing.TEST)
    androidTestImplementation(Deps.Testing.ESPRESSO)

    // Kotlin
    implementation(kotlin(Deps.Kotlin.STDLIB, Deps.Kotlin.VERSION))
    implementation(Deps.Kotlin.COROUTINES)

    // Material Components
    implementation(Deps.Material.COMPONENTS)

    // Jetpack
    implementation(Deps.Jetpack.APPCOMPAT)
    implementation(Deps.Jetpack.PREFERENCE)
    implementation(Deps.Jetpack.CORE_KTX)

    // Tools
    implementation(Deps.Tools.CONSTRAINT_LAYOUT)

    // Android Architecture Components
    implementation(Deps.AAC.Lifecycle.EXTENSIONS)
    kapt(Deps.AAC.Lifecycle.COMPILER)
    implementation(Deps.AAC.Navigation.FRAGMENT_KTX)
    implementation(Deps.AAC.Navigation.UI_KTX)

    // Dagger
    implementation(Deps.Dagger.DAGGER)
    implementation(Deps.Dagger.ANDROID_SUPPORT)
    kapt(Deps.Dagger.COMPILER)
    kapt(Deps.Dagger.ANDROID_PROCESSOR)

    // LeakCanary
    debugImplementation(Deps.LeakCanary.ANDROID)
    releaseImplementation(Deps.LeakCanary.ANDROID_NO_OP)
    debugImplementation(Deps.LeakCanary.SUPPORT_FRAGMENT)

    // Moshi
    implementation(Deps.Moshi.MOSHI)
    kapt(Deps.Moshi.KOTLIN_CODEGEN)

    // Retrofit + ( Converters and Adapters )
    implementation(Deps.Retrofit.RETROFIT)
    implementation(Deps.Retrofit.CONVERTER_SCALARS)
    implementation(Deps.Retrofit.CONVERTER_MOSHI)
    implementation(Deps.Retrofit.ADAPTER_COROUTINES)

    // OkHttp
    implementation(Deps.OkHttp.OKHTTP)

    // Chuck
    debugImplementation(Deps.Chuck.LIBRARY)
    releaseImplementation(Deps.Chuck.LIBRARY_NO_OP)

    // Result
    implementation(Deps.Result.RESULT)

    // Material Dialogs
    implementation(Deps.MaterialDialogs.CORE)
    implementation(Deps.MaterialDialogs.FILES)
    implementation(Deps.MaterialDialogs.INPUT)

    // Dexter
    implementation(Deps.Dexter.DEXTER)
}
