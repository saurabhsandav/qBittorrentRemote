object ProjectConfig {
    const val BUILD_TOOLS_VERSION = "28.0.2"

    const val COMPILE_SDK_VERSION = 28
    const val MIN_SDK_VERSION = 21
    const val TARGET_SDK_VERSION = 28
}

object Deps {

    object Gradle {
        private const val PLUGIN_VERSION = "3.3.0-alpha08"

        const val PLUGIN = "com.android.tools.build:gradle:$PLUGIN_VERSION"
    }

    object Kotlin {
        const val VERSION = "1.2.61"
        private const val COROUTINES_VERSION = "0.24.0"

        const val GRADLE_PLUGIN = "gradle-plugin"
        const val STDLIB = "stdlib-jdk7"
        const val COROUTINES = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$COROUTINES_VERSION"
    }

    object Testing {
        private const val JUNIT_VERSION = "4.12"
        private const val TEST_VERSION = "1.1.0-alpha4"
        private const val ESPRESSO_VERSION = "3.1.0-alpha4"

        const val JUNIT = "junit:junit:$JUNIT_VERSION"
        const val TEST = "androidx.test:runner:$TEST_VERSION"
        const val ESPRESSO = "androidx.test.espresso:espresso-core:$ESPRESSO_VERSION"
    }

    object Material {
        private const val VERSION = "1.0.0-rc01"

        const val COMPONENTS = "com.google.android.material:material:$VERSION"
    }

    object Jetpack {
        private const val VERSION = "1.0.0-rc01"

        const val APPCOMPAT = "androidx.appcompat:appcompat:$VERSION"
        const val PREFERENCE = "androidx.preference:preference:$VERSION"

        const val CORE_KTX = "androidx.core:core-ktx:$VERSION"
    }

    object Tools {
        private const val VERSION = "2.0.0-alpha2"

        const val CONSTRAINT_LAYOUT = "androidx.constraintlayout:constraintlayout:$VERSION"
    }

    object AAC {

        object Lifecycle {
            private const val VERSION = "2.0.0-rc01"

            const val EXTENSIONS = "androidx.lifecycle:lifecycle-extensions:$VERSION"
            const val COMPILER = "androidx.lifecycle:lifecycle-compiler:$VERSION"
        }

        object Navigation {
            private const val VERSION = "1.0.0-alpha05"

            const val FRAGMENT_KTX = "android.arch.navigation:navigation-fragment-ktx:$VERSION"
            const val UI_KTX = "android.arch.navigation:navigation-ui-ktx:$VERSION"
        }
    }

    object Dagger {
        private const val VERSION = "2.16"

        const val DAGGER = "com.google.dagger:dagger:$VERSION"
        const val ANDROID_SUPPORT = "com.google.dagger:dagger-android-support:$VERSION"
        const val COMPILER = "com.google.dagger:dagger-compiler:$VERSION"
        const val ANDROID_PROCESSOR = "com.google.dagger:dagger-android-processor:$VERSION"
    }

    object LeakCanary {
        private const val VERSION = "1.6.1"

        const val ANDROID = "com.squareup.leakcanary:leakcanary-android:$VERSION"
        const val ANDROID_NO_OP = "com.squareup.leakcanary:leakcanary-android-no-op:$VERSION"
        const val SUPPORT_FRAGMENT = "com.squareup.leakcanary:leakcanary-support-fragment:$VERSION"
    }

    object Moshi {
        private const val VERSION = "1.6.0"

        const val MOSHI = "com.squareup.moshi:moshi:$VERSION"
        const val KOTLIN_CODEGEN = "com.squareup.moshi:moshi-kotlin-codegen:$VERSION"
    }

    object Retrofit {
        private const val VERSION = "2.4.0"
        private const val ADAPTER_COROUTINES_VERSION = "1.0.0"

        const val RETROFIT = "com.squareup.retrofit2:retrofit:$VERSION"
        const val CONVERTER_SCALARS = "com.squareup.retrofit2:converter-scalars:$VERSION"
        const val CONVERTER_MOSHI = "com.squareup.retrofit2:converter-moshi:$VERSION"
        const val ADAPTER_COROUTINES = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-experimental-adapter:$ADAPTER_COROUTINES_VERSION"
    }

    object OkHttp {
        private const val VERSION = "3.11.0"

        const val OKHTTP = "com.squareup.okhttp3:okhttp:$VERSION"
    }

    object Chuck {
        private const val VERSION = "1.1.0"

        const val LIBRARY = "com.readystatesoftware.chuck:library:$VERSION"
        const val LIBRARY_NO_OP = "com.readystatesoftware.chuck:library-no-op:$VERSION"
    }

    object Result {
        private const val VERSION = "1.5.0"

        const val RESULT = "com.github.kittinunf.result:result:$VERSION"
    }

    object MaterialDialogs {
        private const val VERSION = "2.0.0-alpha05"

        const val CORE = "com.afollestad.material-dialogs:core:$VERSION"
        const val FILES = "com.afollestad.material-dialogs:files:$VERSION"
        const val INPUT = "com.afollestad.material-dialogs:input:$VERSION"
    }

    object Dexter {
        private const val VERSION = "5.0.0"

        const val DEXTER = "com.karumi:dexter:$VERSION"
    }
}
